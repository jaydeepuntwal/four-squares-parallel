//******************************************************************************
//
// File:    FourSquaresSmp.java
// 
// This Java source file uses the Parallel Java 2 Library ("PJ2") developed by
// Prof. Alan Kaminsky (RIT).
//
//******************************************************************************

import edu.rit.pj2.IntVbl;
import edu.rit.pj2.Loop;
import edu.rit.pj2.Task;

/**
 * Class FourSquaresSmp is a parallel program that implements Lagrange's Four
 * Squares Theorem for Integer <N> and prints the lexicographically largest set
 * of numbers whose sum of squares is equal to <N>. It also calculates the total
 * number of such sets.
 * 
 * Usage: <TT>java pj2 FourSquaresSmp <I>number</I> ...</TT>
 * 
 * @author Jaydeep Untwal (jmu8722@cs.rit.edu)
 * @version 19-Sep-2014
 */
public class FourSquaresSmp extends Task {

	// Class Variable: Max Set and Count
	FourSqSet fsMax;
	IntVbl count;

	/*
	 * Main Program
	 * 
	 * @param args[0] = Integer (n) >= 0
	 */
	public void main(String[] args) {

		// Validate command line argument
		if (args.length >= 1) {

			// Check if Integer
			// Throw new IllegalArgumentException
			try {
				Integer.parseInt(args[0]);
			} catch (Exception e) {
				usage();
			}

			final int n = Integer.parseInt(args[0]);

			// Validate input
			if (n >= 0) {

				// Initialize Class Variables
				fsMax = new FourSqSet(0, 0, 0, 0);
				count = new IntVbl.Sum(0);

				// Search for four squares in parallel with a dynamic schedule
				parallelFor(0, (int) Math.sqrt(n)).schedule(dynamic).exec(
						new Loop() {

							// Local thread max and count
							FourSqSet thrMax;
							IntVbl thrCount;

							// Start
							public void start() {
								thrCount = threadLocal(count);
								thrMax = threadLocal(fsMax);
							}

							// Run Method
							@Override
							public void run(int i) throws Exception {
								for (int j = i; (j * j) <= n; j++) {

									for (int k = j; (k * k) <= n; k++) {

										for (int l = k; (l * l) <= n; l++) {

											// Check sum of squares
											int sum = (i * i) + (j * j)
													+ (k * k) + (l * l);

											// Reduce and increment count
											if (n == sum) {
												FourSqSet candidate = new FourSqSet(
														i, j, k, l);
												thrCount.item++;
												thrMax.reduce(candidate);
											}
										}
									}
								}
							}

						});

				// Print results
				System.out.println(n + " = " + fsMax);
				System.out.println(count);
			} else {
				usage();
			}
		} else {
			usage();
		}
	}

	// Hidden operations.

	/**
	 * Print a usage message and exit.
	 * 
	 * @throws IllegalArgumentException
	 */
	private static void usage() {
		System.err.println("Usage: java pj2 FourSquaresSmp <N>");
		System.err.println("<N> = Number");
		throw new IllegalArgumentException();
	}

}
