//******************************************************************************
//
// File:    FourSqSet.java
// 
// This Java source file uses the Parallel Java 2 Library ("PJ2") developed by
// Prof. Alan Kaminsky (RIT).
//
//******************************************************************************

import edu.rit.pj2.Vbl;

/**
 * Class FourSqSet is a set of four numbers which implements Vbl interface The
 * four numbers are displayed as set of sum of four squares
 * 
 * Usage: FourSqSet( int, int , int , int )
 * 
 * @author Jaydeep Untwal (jmu8722@cs.rit.edu)
 * @version 19-Sep-2014
 */
public class FourSqSet implements Vbl {

	// Four Numbers
	int a, b, c, d;

	/*
	 * Parametereized constructor
	 */
	FourSqSet(int a, int b, int c, int d) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}

	/*
	 * toString
	 * 
	 * @return Return set of suares of four numbers as String
	 */
	public String toString() {
		return a + "^2 + " + b + "^2 + " + c + "^2 + " + d + "^2";
	}

	/*
	 * Return a clone of this object
	 * 
	 * @return - new FourSqSet Object
	 */
	public Object clone() {
		return new FourSqSet(a, b, c, d);
	}

	/*
	 * Reduce using lexicographical order
	 * 
	 * @param arg0 - FourSqSet
	 */
	@Override
	public void reduce(Vbl arg0) {

		FourSqSet fs = (FourSqSet) arg0;

		if (this.a < fs.a) {
			this.set(arg0);
		} else if (this.a == fs.a) {
			if (this.b < fs.b) {
				this.set(arg0);
			} else if (this.b == fs.b) {
				if (this.c < fs.c) {
					this.set(arg0);
				} else if (this.c == fs.c) {
					if (this.d < fs.d) {
						this.set(arg0);
					} else {
						return;
					}
				} else {
					return;
				}
			} else {
				return;
			}
		} else {
			return;
		}

	}

	/*
	 * Set self variables to another's
	 * 
	 * @param arg0 - FourSqSet
	 */
	@Override
	public void set(Vbl arg0) {
		FourSqSet fs = (FourSqSet) arg0;

		this.a = fs.a;
		this.b = fs.b;
		this.c = fs.c;
		this.d = fs.d;

	}

}